use role accountadmin;
use warehouse compute_wh;
use database weather;
use schema public;

drop share if exists trips_share;
drop database if exists citibike;
drop database if exists weather;
drop warehouse if exists analytics_wh;
drop role if exists junior_dba;